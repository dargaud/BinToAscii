///////////////////////////////////////////////////////////////////////////////////////////////////
// PROGRAM:   BinToAscii.exe                                                        
//
// VERSION:   1.10
//
// COPYRIGHT: � 1999-2012 Guillaume Dargaud - Freeware                                 
//
// PURPOSE:   Conversion of arbitrary sequential binary files to text files
//            The binary files are assumed to have a format such as:
//              - Header
//              - Record 1
//              - Record 2
//              - ...
//              - Record n
//            Each containing a succession of bytes, short, long, float or double value
//
// INSTALL:   run setup.exe 
//
// HELP:      available by right-clicking an item on the user interface.
//
// TUTORIAL:  http://www.gdargaud.net/Hack/BinToAscii.html
//
// HISTORY:   v1.0 - 99/06/22 - Original release
//            v1.3 - 99/12/16 - Modified to allow any number of formats (saves them in INI files)
//            v1.4 - 01/06/12 - Added byte swapping option
//            v1.5 - 01/06/27 - Corrected byte swapping bug, 
//                              added string support, 
//                              much faster display,
//                              unlimited string size,
//                              repeat numbers
//            v1.6 - 01/06/28 - Error checking on output format
//            v1.7 - 02/03/22 - Minor internal enhancements.
//            v1.8 - 02/10/04 - Added Y, H and U for unsigned byte, short and long
//            v1.9 - 02/11/04 - Added string delimiters
//            v1.10- 11/12/09 - Added more explicit sizes, 8-byte longs and various levels of swap
//
// KNOWN BUGS:- no possibility of mixing outputs for, say, %x%d%u results.
//            - the L/U are overriden by the choice of %d or %u for output. Leave ouput field blank if mixed of U and L.
//            - not sure about double number swapping, if it should be 8 bytes or 4. Probably processor dependant. Set to 16.
//            - No error checking on output string qualifier. If you put %s%c instead of %d, don't be surprised of the results
//
// TO DO: Save custom formats in registry
//
// MONEY: It is a free program, but if your wallet is too big and you tilt sideways when you sit down,
//        you can send your extra US$/FF/ITL to:
//        http://s1.amazon.com/exec/varzea/pay/T2V0DRWPA7EAJ8
///////////////////////////////////////////////////////////////////////////////////////////////////

#define _VER_ "1.10"			// Version
#define _WEB_ "http://www.gdargaud.net/Hack/BinToAscii.html"
#define _COPY_ "� 1999-2012 Guillaume Dargaud"


#include <cvintwrk.h>
#include <formatio.h>
#include <ansi_c.h>
#include <cvirte.h>		/* Needed if linking in external compiler; harmless otherwise */
#include <userint.h>
#include <utility.h>
#include "toolbox.h"

#include "BinToAscii.h"
#include "Def.h"


static char CopyRight[]="BinToAscii c" _VER_ " - "_COPY_"\n"_WEB_;

static int Pnl=0, Inp=0, Menu=0;

static int NbFiles=0, CurrentFile=-1;
static char **FileList=NULL;
static BOOL SaveText=FALSE;
static int SwapEndian=0;
static int TextFile=0;

#define MAX_LEN 1000
static char TypeString[19]="",
			*PreHeaderString=NULL, 		*PreFormatString=NULL,
			*HeaderString=NULL, 		*FormatString=NULL,
			*HeaderBufferString=NULL,	*FormatBufferString=NULL;
static int  MaxLen=0, HeaderBufferLen=0, FormatBufferLen=0;

static int FH=0;
static BOOL Error=FALSE;
static int RecordNb=0;
static int Index=0;

static char StrStartDelim[MAX_LEN]="\"", StrEndDelim[MAX_LEN]="\"";

///////////////////////////////////////////////////////////////////////////////
/// HIFN	String allocation
/// HIPAR	NewSize/If NewSize==0 or < MaxLen, then increase MaxLen by MAX_LEN
///////////////////////////////////////////////////////////////////////////////
static void ReallocStrings(const int NewSize) {
	int PrevLen=MaxLen;
	if (NewSize>MaxLen) MaxLen=NewSize; else MaxLen+=MAX_LEN;
	
	PreHeaderString    = (char*)realloc (PreHeaderString, MaxLen);
	PreFormatString    = (char*)realloc (PreFormatString, MaxLen);
	HeaderString       = (char*)realloc (HeaderString, MaxLen);
	FormatString       = (char*)realloc (FormatString, MaxLen);
	HeaderBufferString = (char*)realloc (HeaderBufferString, MaxLen);
	FormatBufferString = (char*)realloc (FormatBufferString, MaxLen);
	if (PrevLen==0) HeaderString[0]=FormatString[0]='\0';
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Swap bytes in a block
/// HIPAR	Nb/Number of bytes to swap: 2, 4, 8 or 16
/// HIPAR	Bl/Perform the swap by blocks of 2, 4, 8 or 16
/// HIPAR	Bl/Possible values: 2 for Nb=2, 2/4 for Nb=4, 2/4/8 for Nb=8, 2/4/8/16 for Nb=16
/// HIFN	For instance on 0123456789abcdef Nb=16,Bl=16 will return fedcba9876543210
/// HIFN	Nb=16,Bl=2 will return 1032547698badcfe
///	HIRET	Return a pointer to a static address to the swapped equivalent of Addr. 
///////////////////////////////////////////////////////////////////////////////
static void *Swap(void* Src, const int Nb, const int Bl) {
	static char Swapped[16];
	char *Addr=(char*)Src;
	BOOL Bl2=((Bl==2 or Bl==4 or Bl==8 or Bl==16) and (Nb==2 or Nb==4 or Nb==8 or Nb==16)),
		 Bl4=((         Bl==4 or Bl==8 or Bl==16) and (         Nb==4 or Nb==8 or Nb==16)),
		 Bl8=((                  Bl==8 or Bl==16) and (                  Nb==8 or Nb==16)),
		Bl16=((                           Bl==16) and (                           Nb==16));
	switch (Nb) {
		case 1:	Swapped[ 0]=Addr[0];
				break;
		case 2:	Swapped[ 0]=Addr[1];
				Swapped[ 1]=Addr[0];
				break;
		case 4:	Swapped[ 0]=Addr[Bl4?3:1];
				Swapped[ 1]=Addr[Bl4?2:0];
				Swapped[ 2]=Addr[Bl4?1:3];
				Swapped[ 3]=Addr[Bl4?0:2];
				break;
		case 8:	Swapped[ 0]=Addr[Bl8?7:Bl4?3:1];
				Swapped[ 1]=Addr[Bl8?6:Bl4?2:0];
				Swapped[ 2]=Addr[Bl8?5:Bl4?1:3];
				Swapped[ 3]=Addr[Bl8?4:Bl4?0:2];
				Swapped[ 4]=Addr[Bl8?3:Bl4?7:5];
				Swapped[ 5]=Addr[Bl8?2:Bl4?6:4];
				Swapped[ 6]=Addr[Bl8?1:Bl4?5:7];
				Swapped[ 7]=Addr[Bl8?0:Bl4?4:6];
				break;
		case 16:Swapped[ 0]=Addr[Bl16?15:Bl8? 7:Bl4? 3: 1];
				Swapped[ 1]=Addr[Bl16?14:Bl8? 6:Bl4? 2: 0];
				Swapped[ 2]=Addr[Bl16?13:Bl8? 5:Bl4? 1: 3];
				Swapped[ 3]=Addr[Bl16?12:Bl8? 4:Bl4? 0: 2];
				Swapped[ 4]=Addr[Bl16?11:Bl8? 3:Bl4? 7: 5];
				Swapped[ 5]=Addr[Bl16?10:Bl8? 2:Bl4? 6: 4];
				Swapped[ 6]=Addr[Bl16? 9:Bl8? 1:Bl4? 5: 7];
				Swapped[ 7]=Addr[Bl16? 8:Bl8? 0:Bl4? 4: 6];
				Swapped[ 8]=Addr[Bl16? 7:Bl8?15:Bl4?11: 9];
				Swapped[ 9]=Addr[Bl16? 6:Bl8?14:Bl4?10: 8];
				Swapped[10]=Addr[Bl16? 5:Bl8?13:Bl4? 9:11];
				Swapped[11]=Addr[Bl16? 4:Bl8?12:Bl4? 8:10];
				Swapped[12]=Addr[Bl16? 3:Bl8?11:Bl4?15:13];
				Swapped[13]=Addr[Bl16? 2:Bl8?10:Bl4?14:12];
				Swapped[14]=Addr[Bl16? 1:Bl8? 9:Bl4?13:15];
				Swapped[15]=Addr[Bl16? 0:Bl8? 8:Bl4?12:14];
				break;
	}
	return (void*)Swapped;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Convert char to '.' if non printable
///////////////////////////////////////////////////////////////////////////////
char ToAsciiChar(char C) {
	return isprint(C) ? C : '.';
}

// Extend a format string with 'll'
char *LLEXT(char *Frmt, char *Ext) {
	static char Result[600];
	int Pos=strlen(Frmt)-1,	// Position of the 'd' in %d
		Len=strlen(Ext);
	strcpy(Result, Frmt);
	if (Pos<1) return Result;	// No format ?
	Result[Pos+Len+1]='\0';
	Result[Pos+Len]=Result[Pos];
	strncpy(Result+Pos, Ext, Len);
	return Result;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Display a record
///////////////////////////////////////////////////////////////////////////////
static void DisplayRecord(char* String, int StringLen, char* BufferPtr) {
	void *Ptr=BufferPtr;
	int i, TabSeparator;
	static char Str[600], IntFrmt[600], FltFrmt[600], Transit[600];
	char *Separator;
	BOOL FirstChar, LastChar, PrintedSomething=FALSE;
	
	if (FH<1 || Error || StringLen==0) return;

	if (BufferPtr==NULL) return;
	GetCtrlVal(Pnl, PNL_INT_FORMAT,   IntFrmt);
	GetCtrlVal(Pnl, PNL_FLOAT_FORMAT, FltFrmt);
	GetCtrlVal(Pnl, PNL_SEPARATOR,   &TabSeparator);

	#define  INTFRMT (IntFrmt[0]=='\0' ? "%d" : IntFrmt)
	#define  UNTFRMT (IntFrmt[0]=='\0' ? "%u" : IntFrmt)
	#define LINTFRMT LLEXT(INTFRMT, "ll")
	#define LUNTFRMT LLEXT(UNTFRMT, "ll")
	#define LFLFFRMT LLEXT(FltFrmt, "L")

	if (strlen(FltFrmt)==0) strcpy(FltFrmt, "%g");

	for (i=0; i<StringLen; i++) {
		switch (String[i]) {
			case 'B': sprintf(Str, INTFRMT, *(              char*)                                       Ptr);  Ptr=(              char*)Ptr+1; break;
			case 'Y': sprintf(Str, UNTFRMT, *(unsigned      char*)                                       Ptr);  Ptr=(unsigned      char*)Ptr+1; break;
			case 'C': sprintf(Str, "%c", ToAsciiChar(*(     char*)                                       Ptr)); Ptr=(              char*)Ptr+1; break;
			case 'S': sprintf(Str, INTFRMT, *(             short*)(SwapEndian ? Swap(Ptr, 2,2         ): Ptr)); Ptr=(unsigned     short*)Ptr+1; break;
			case 'H': sprintf(Str, UNTFRMT, *(unsigned     short*)(SwapEndian ? Swap(Ptr, 2,2         ): Ptr)); Ptr=(             short*)Ptr+1; break;
			case 'L': sprintf(Str, INTFRMT, *(              long*)(SwapEndian ? Swap(Ptr, 4,SwapEndian): Ptr)); Ptr=(              long*)Ptr+1; break;
			case 'U': sprintf(Str, UNTFRMT, *(unsigned      long*)(SwapEndian ? Swap(Ptr, 4,SwapEndian): Ptr)); Ptr=(unsigned      long*)Ptr+1; break;
			case 'T': sprintf(Str,LINTFRMT, *(         long long*)(SwapEndian ? Swap(Ptr, 8,SwapEndian): Ptr)); Ptr=(         long long*)Ptr+1; break;
			case 'N': sprintf(Str,LUNTFRMT, *(unsigned long long*)(SwapEndian ? Swap(Ptr, 8,SwapEndian): Ptr)); Ptr=(unsigned long long*)Ptr+1; break;
			case 'F': sprintf(Str, FltFrmt, *(             float*)(SwapEndian ? Swap(Ptr, 4,SwapEndian): Ptr)); Ptr=(             float*)Ptr+1; break;
			case 'D': sprintf(Str, FltFrmt, *(            double*)(SwapEndian ? Swap(Ptr, 8,SwapEndian): Ptr)); Ptr=(            double*)Ptr+1; break;
//			case 'E': sprintf(Str,LFLFFRMT, *(       long double*)(SwapEndian ? Swap(Ptr,16,SwapEndian): Ptr)); Ptr=(       long double*)Ptr+1; break;
			case 'I': 
			default : Str[0]='\0'; Ptr=(char*)Ptr+1; break;
		}

		FirstChar= String[i]=='C' and (i==0           or i>0           and String[i-1]!='C');
		LastChar = String[i]=='C' and (i==StringLen-1 or i<StringLen-1 and String[i+1]!='C');
		
		Transit[0]='\0';
		
		Separator = TabSeparator ? (SaveText ? "\t" : " ") : ", ";
		if (i==0) {
			if (RecordNb>=0 /*>1*/) strcpy(Transit, "\n");
		}
		else if (PrintedSomething and String[i]!='I' and (String[i]!='C' or String[i-1]!='C')) strcat(Transit, Separator);
		if (FirstChar) 		strcat(Transit, StrStartDelim);
		strcat(Transit, Str);
		if (LastChar) 		strcat(Transit, StrEndDelim);

		if (Transit[0]!='\0') {
			PrintedSomething = (PrintedSomething or strcmp(Transit, "\n")!=0);
			if (SaveText) WriteFile (TextFile, Transit, strlen(Transit));
			else SetCtrlVal(Pnl, PNL_ASCII_VIEW, Transit);
		}
	}
}



static BOOL OpenNextFile(void);

static long BytesReadForThisFile=0, SizeOfCurrentFile=0;

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Read and convert the header from a file
///////////////////////////////////////////////////////////////////////////////
static BOOL ReadHeader(void) {
	int BytesRead;
	int OK;
	if (FH<1) return FALSE;
	
	if (!SaveText) ResetTextBox (Pnl, PNL_ASCII_VIEW, "");
	SetFilePtr (FH, 0, 0);
	BytesRead=ReadFile (FH, HeaderBufferString, HeaderBufferLen);
	BytesReadForThisFile+=BytesRead;
	if (BytesRead<HeaderBufferLen) {
		CloseFile(FH);
		FH=-1;
		BytesReadForThisFile=0;
		OK=OpenNextFile();
		return OK;
	}
	RecordNb=0;
	if (!SaveText) SetCtrlVal(Pnl, PNL_ASCII_VIEW, "Header=");
	DisplayRecord(HeaderString, strlen(HeaderString), HeaderBufferString);
	return TRUE;
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Read and convert the next record from a file
///////////////////////////////////////////////////////////////////////////////
static BOOL ReadNextRecord(void) {
	int BytesRead;
	int OK;
	static char Str[100];
	if (FH<1) return FALSE;
	
	BytesRead=ReadFile (FH, FormatBufferString, FormatBufferLen);
	BytesReadForThisFile+=BytesRead;
	if (BytesRead<FormatBufferLen) {
		CloseFile(FH);
		FH=-1;
		BytesReadForThisFile=0;
		OK=OpenNextFile();
		return OK;
	}
	sprintf(Str, "\nRec[%d]=", ++RecordNb);
	if (!SaveText) SetCtrlVal(Pnl, PNL_ASCII_VIEW, Str);
	DisplayRecord(FormatString, strlen(FormatString), FormatBufferString);
	return TRUE;
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Open a new file in a series
///////////////////////////////////////////////////////////////////////////////
static BOOL OpenNextFile(void) {
	char Str[150];
	if (++CurrentFile==NbFiles) {
		SetCtrlVal(Pnl, PNL_FILE_NAME, "No File selected.");
		SetCtrlAttribute(Pnl, PNL_FILE_SELECT, ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(Pnl, PNL_READ_ALL, ATTR_DIMMED, TRUE);
		SetCtrlAttribute(Pnl, PNL_NEXT_REC, ATTR_DIMMED, TRUE);
		SetCtrlAttribute(Pnl, PNL_CANCEL, ATTR_VISIBLE, FALSE);
		SetMenuBarAttribute (Menu, MENU_FILE_SELECT, ATTR_DIMMED, FALSE);
		CurrentFile=-1;
		return FALSE;
	}
	FH = OpenFile (FileList[CurrentFile], VAL_READ_ONLY, VAL_OPEN_AS_IS, VAL_BINARY);
	if (FH==-1) {
		sprintf(Str, "\nCould not open file %s", FileList[CurrentFile]);
		SetCtrlVal(Pnl, PNL_ASCII_VIEW, Str);
		return FALSE;
	}
	if (1 != GetFileInfo (FileList[CurrentFile], &SizeOfCurrentFile)) SizeOfCurrentFile=0;
	SetCtrlVal(Pnl, PNL_FILE_NAME, FileList[CurrentFile]);
	if (!ReadHeader()) return FALSE;
	return ReadNextRecord();
}



///////////////////////////////////////////////////////////////////////////////
/// HIFN	Read configuration file
///////////////////////////////////////////////////////////////////////////////
void ReadIniFile(void) {
	char *Str;
	int Cnt=0, St, Size;
	int InitFile=-1;

	SetCtrlVal(Pnl, PNL_COPYRIGHT, CopyRight);
	
	if (0!=GetFileSize ("BinToAscii.ini", &Size)) return;
	Str=Calloc(Size, char);
	
	ClearListCtrl (Pnl, PNL_TYPE);
	ClearListCtrl (Pnl, PNL_HEADER);
	ClearListCtrl (Pnl, PNL_RECORDS);
	InitFile = OpenFile ("BinToAscii.ini", VAL_READ_ONLY, VAL_OPEN_AS_IS, VAL_ASCII);
	if (InitFile<=0) return;
	
	while ( (St=ReadLine (InitFile, Str, Size-1))>0) {
		InsertListItem (Pnl, PNL_TYPE, -1, Str, Cnt);
		ReadLine (InitFile, Str, Size-1);
		InsertListItem (Pnl, PNL_HEADER, -1, Str, Cnt);
		ReadLine (InitFile, Str, Size-1);
		InsertListItem (Pnl, PNL_RECORDS, -1, Str, Cnt);
		Cnt++;
	}
	CloseFile(InitFile);
	GetNumListItems (Pnl, PNL_TYPE, &Cnt);

	free(Str);
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Save configuration file
///////////////////////////////////////////////////////////////////////////////
void SaveIniFile(void) {
	char *Str;
	int Cnt=0, i;
	int InitFile=-1;

	InitFile = OpenFile ("BinToAscii.ini", VAL_WRITE_ONLY, VAL_TRUNCATE, VAL_ASCII);
	if (InitFile<=0) return;
	Str=Calloc(MaxLen+1, char);
	
	GetNumListItems (Pnl, PNL_TYPE, &Cnt);
	for (i=0; i<Cnt; i++) {
		GetLabelFromIndex (Pnl, PNL_TYPE, i, Str);
		WriteLine (InitFile, Str, -1);
		GetLabelFromIndex (Pnl, PNL_HEADER, i, Str);
		WriteLine (InitFile, Str, -1);
		GetLabelFromIndex (Pnl, PNL_RECORDS, i, Str);
		WriteLine (InitFile, Str, -1);
	}
	CloseFile(InitFile);
	free(Str);
}




///////////////////////////////////////////////////////////////////////////////
/// HIFN	main
///////////////////////////////////////////////////////////////////////////////
int main (int argc, char *argv[]) {
	if (InitCVIRTE (0, argv, 0) == 0)	/* Needed if linking in external compiler; harmless otherwise */
		return -1;	/* out of memory */
	Pnl = LoadPanel (0, "BinToAscii.uir", PNL);
	Inp = LoadPanel (0, "BinToAscii.uir", INP);
	Menu = GetPanelMenuBar (Pnl);
	if (Pnl<0 or Inp<0 or Menu<0) return -1;
	cbp_Panel(Pnl, EVENT_PANEL_SIZE, NULL, 0, 0);

	DisplayPanel (Pnl);

	ReallocStrings(0);
	ReadIniFile();

	RunUserInterface ();
	
	SaveIniFile();
	DiscardPanel(Pnl);
	DiscardPanel(Inp);
	return 0;
}




///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK CB_Quit (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			QuitUserInterface (0);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Quit", "What did you seriously expect to see here ?");
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Multiple file selection
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK CB_FileSelect (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int i, St;
	switch (event) {
		case EVENT_COMMIT:
			if (FileList!=NULL) 
				for (i=0; i<NbFiles; i++)
					if (FileList[i]!=NULL) {
						free(FileList[i]);
						FileList[i]=NULL;
					}
			free(FileList); FileList=NULL;

			St = MultiFileSelectPopup ("", "*.*", "?????met*.dat; ?????usa*.*; *.bin; *.dat",
									   "Select binary files to convert", 0, 0, 1,
									   &NbFiles, &FileList);
			
			if (St==VAL_EXISTING_FILE_SELECTED) {
				CurrentFile=-1;
				SetCtrlAttribute(Pnl, PNL_FILE_SELECT, ATTR_VISIBLE, FALSE); 
				SetCtrlAttribute(Pnl, PNL_READ_ALL, ATTR_DIMMED, FALSE);
				SetCtrlAttribute(Pnl, PNL_NEXT_REC, ATTR_DIMMED, FALSE);
				SetCtrlAttribute(Pnl, PNL_CANCEL, ATTR_VISIBLE, TRUE);
				SetMenuBarAttribute (Menu, MENU_FILE_SELECT, ATTR_DIMMED, TRUE);

				SetCtrlAttribute(Pnl, PNL_ASCII_VIEW, ATTR_VISIBLE, FALSE);
				OpenNextFile();
				SetCtrlAttribute(Pnl, PNL_ASCII_VIEW, ATTR_VISIBLE, TRUE);
			}
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("File select", "Select a list of binary files to convert to ASCII\n"
										"The first file will be used for the test of the format string.");
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Count the number of bytes of an conversion string
///////////////////////////////////////////////////////////////////////////////
static int ExpansionCount(char* Format) {
	int FormatStringLen, FormatBufferLen=0, i;
	Error=FALSE;
	FormatStringLen=strlen(Format);

	for (i=0; i<FormatStringLen; i++) 
		switch (toupper(Format[i])) {
			case ' ':break;
			case 'I':
			case 'C':
			case 'B': 
			case 'Y': FormatBufferLen++;  break;
			case 'S': 
			case 'H': FormatBufferLen+=2; break;
			case 'L': 
			case 'U':
			case 'F': FormatBufferLen+=4; break;
			case 'T':
			case 'N':
			case 'D': FormatBufferLen+=8; break;
//			case 'E': FormatBufferLen+=16; break;
			default:  Format[i]='?'; Error=TRUE; break;
		}
	
	SetCtrlVal(Pnl, PNL_ERROR, Error);
	return FormatBufferLen;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Count the number of items in a format string
/// HIPAR	Format/Input string contains spaces and numbers, count valid char
///////////////////////////////////////////////////////////////////////////////
static int PreExpansionCount(char* Format) {
	int FormatStringLen, FormatLen=0, i, Last=0;

	Error=FALSE;
	FormatStringLen=strlen(Format);
	
	for (i=0; i<FormatStringLen; i++) 
		switch (toupper(Format[i])) {
			case ' ': Last=0; break;
			case 'I':
			case 'C':
			case 'B': 
			case 'Y': 
			case 'S': 
			case 'H': 
			case 'L': 
			case 'U': 
			case 'F': 
			case 'D': 
			case 'N': 
			case 'T': /*
			case 'E': */FormatLen+= (Last=1); break;
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9': if (Last==1) FormatLen+= atoi(&Format[i])-1; 
						Last=0; break;
			default : Format[i]='?'; Error=TRUE; Last=0; break;
		}
	
	SetCtrlVal(Pnl, PNL_ERROR, Error);
	return FormatLen;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Input string contains spaces and numbers, count valid char
///////////////////////////////////////////////////////////////////////////////
static void Expand(char* Dest, char* Source) {
	int SourceLen, FormatLen=0, i, j, Last=0, Pos=0, Nb;
	char Action;
	
	Error=FALSE;
	SourceLen=strlen(Source);
	
	for (i=0; i<SourceLen; i++) 
		switch (toupper(Source[i])) {
			case ' ': Last=0; break;
			case 'I': Dest[Pos++]='I'; Last=1; break;
			case 'C': Dest[Pos++]='C'; Last=1; break;
			case 'B': Dest[Pos++]='B'; Last=1; break;
			case 'Y': Dest[Pos++]='Y'; Last=1; break;
			case 'S': Dest[Pos++]='S'; Last=1; break;
			case 'H': Dest[Pos++]='H'; Last=1; break;
			case 'L': Dest[Pos++]='L'; Last=1; break;
			case 'U': Dest[Pos++]='U'; Last=1; break;
			case 'T': Dest[Pos++]='T'; Last=1; break;
			case 'N': Dest[Pos++]='N'; Last=1; break;
			case 'F': Dest[Pos++]='F'; Last=1; break;
			case 'D': Dest[Pos++]='D'; Last=1; break;
//			case 'E': Dest[Pos++]='E'; Last=1; break;
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':	if (Last==1 and Pos>0) {
							Nb=atoi(&Source[i]);
							Action=Dest[Pos-1];
							for (j=1; j<Nb; j++) Dest[Pos++] = Action;
							Last=0;
						}
						break;
			default:  Source[i]='?'; Error=TRUE; Last=0; break;
		}
	Dest[Pos++]='\0';
	SetCtrlVal(Pnl, PNL_ERROR, Error);
}




///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK CB_Cancel (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			SetCtrlAttribute(Pnl, PNL_FILE_SELECT, ATTR_VISIBLE, TRUE);
			SetCtrlAttribute(Pnl, PNL_READ_ALL, ATTR_DIMMED, TRUE);
			SetCtrlAttribute(Pnl, PNL_NEXT_REC, ATTR_DIMMED, TRUE);
			SetCtrlAttribute(Pnl, PNL_CANCEL, ATTR_VISIBLE, FALSE);
			SetMenuBarAttribute (Menu, MENU_FILE_SELECT, ATTR_DIMMED, FALSE);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Cancel", "Reset the file list.");
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Read and convert all the files
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK CB_ReadAll (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int Progress, Res;
	static char DestFile[MAX_PATHNAME_LEN]="BinToAscii.txt", Str[MAX_PATHNAME_LEN+20];
	switch (event) {
		case EVENT_COMMIT:
			Res = FileSelectPopup (DestFile, "*.*", "*.bin2ascii; *.txt; *.ascii",
								   "Select Text file to create", VAL_SAVE_BUTTON,
								   0, 0, 1, 1, DestFile);
			if (Res!=VAL_EXISTING_FILE_SELECTED and Res!=VAL_NEW_FILE_SELECTED) return 0;
			
			SetWaitCursor (1);
			SaveText=TRUE;
			CurrentFile=-1;		// Restart from the beginning
			TextFile = OpenFile (DestFile, VAL_WRITE_ONLY, VAL_TRUNCATE, VAL_ASCII);
			OpenNextFile();
			Progress = CreateProgressDialog ("Completion Status", "Percent Complete", 1, VAL_FULL_MARKERS, "__Abort");

			while (ReadNextRecord() and 
				! UpdateProgressDialog (Progress, (100*CurrentFile+100*BytesReadForThisFile/(SizeOfCurrentFile+1))/NbFiles, 1) );
			
			DiscardProgressDialog (Progress);
			SaveText=FALSE;
			CloseFile (TextFile);
			sprintf(Str, "\n\nRead and converted all files to \"%s\"", DestFile);
			SetCtrlVal(Pnl, PNL_ASCII_VIEW, Str);
			sprintf(Str, "NotePad %s", DestFile);
			LaunchExecutable(Str);
			SetWaitCursor (0);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Read All", "Restart from the begining and convert\n"
									"all the selected binary files to a single\n"
									"text file, using the required format.");
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK CB_NextRec (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			SetCtrlAttribute(Pnl, PNL_ASCII_VIEW, ATTR_VISIBLE, FALSE);
			ReadNextRecord();
			SetCtrlAttribute(Pnl, PNL_ASCII_VIEW, ATTR_VISIBLE, TRUE);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Read Next", "Read the next record of the file.\n"
									"If the file is finished, open the next file.");
			break;
	}
	return 0;
}



///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cbp_Panel (int panel, int event, void *callbackData,
		int eventData1, int eventData2) {
	int Height, Width, Left, Top, cWidth;
	switch (event) {
		case EVENT_KEYPRESS:
			if (eventData1==VAL_F1_VKEY) 
				MessagePopup("Bin to Ascii file converter", 
							"(c) 1998-2012 Guillaume Dargaud (freeware)\n"
							"Free use and distribution\n\n"
							"This program uses a format string to determine and convert\n"
							"a series of binary files to text files.");
			break;
		case EVENT_PANEL_SIZE:
			GetPanelAttribute(panel, ATTR_HEIGHT, &Height);
			GetPanelAttribute(panel, ATTR_WIDTH, &Width);
			if (Height<210) SetPanelAttribute(panel, ATTR_HEIGHT, Height=210);
			if (Width <300) SetPanelAttribute(panel, ATTR_WIDTH,  Width =300);

			GetCtrlAttribute(panel, PNL_RECORDS, ATTR_LEFT, &Left);
			SetCtrlAttribute(panel, PNL_RECORDS, ATTR_WIDTH, Width-Left-2);
			
			GetCtrlAttribute(Pnl, PNL_ASCII_VIEW, ATTR_TOP, &Top);
			SetCtrlAttribute(Pnl, PNL_ASCII_VIEW, ATTR_HEIGHT, Height-Top-2);

			GetCtrlAttribute(Pnl, PNL_ASCII_VIEW, ATTR_LEFT, &Left);
			SetCtrlAttribute(Pnl, PNL_ASCII_VIEW, ATTR_WIDTH, Width-Left-2);
			
			GetCtrlAttribute(panel, PNL_FILE_NAME, ATTR_LEFT, &Left);
			SetCtrlAttribute(panel, PNL_FILE_NAME, ATTR_WIDTH, Width-Left-2);
			
			GetCtrlAttribute(panel, PNL_ERROR, ATTR_WIDTH, &cWidth);
			SetCtrlAttribute(panel, PNL_ERROR, ATTR_LEFT, Width-cWidth-2);
			
			GetCtrlAttribute(panel, PNL_RECORD_LENGTH, ATTR_WIDTH, &cWidth);
			SetCtrlAttribute(panel, PNL_RECORD_LENGTH, ATTR_LEFT, Width-cWidth-2);
			
			GetCtrlAttribute(panel, PNL_SWAP_ENDIAN, ATTR_WIDTH, &cWidth);
			SetCtrlAttribute(panel, PNL_SWAP_ENDIAN, ATTR_LEFT, Width-cWidth-2);
			
			GetCtrlAttribute(panel, PNL_SIZES, ATTR_LEFT, &Left);
			SetCtrlAttribute(panel, PNL_SIZES, ATTR_WIDTH, Width-Left-2);
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK CB_IntFormat (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	char Str[600]="", *Pos=NULL;
	
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(Pnl, PNL_INT_FORMAT, Str);
			// If empty, then defaults to %d/%u
			// Should also check if more than one % and the other ones are not %%
			while ((Pos=strstr(Str, "%%"))!=NULL) *Pos=*Pos++=' ';
			if ((Pos=strchr(Str, '%'))==NULL 		// No % sign
				or strchr(Pos+1, '%')!=NULL 			// More than one  
				or strpbrk (Pos+1, "diouxX")==NULL )		// No type char following
				SetCtrlVal(Pnl, PNL_INT_FORMAT, strcpy(Str, ""));	// Reset

			SetCtrlAttribute(Pnl, PNL_ASCII_VIEW, ATTR_VISIBLE, FALSE);
			ReadHeader();
			ReadNextRecord();
			SetCtrlAttribute(Pnl, PNL_ASCII_VIEW, ATTR_VISIBLE, TRUE);
		break;		
		case EVENT_RIGHT_CLICK:
			MessagePopup("Integer output format Override", 
							"Format string for the integers output to the text file\n"
							"Default is blank\n"
							"If blank then B/S/L will default to %d (signed) integer\n"
							"and Y/H/U will default to %u (unsigned) integer.\n\n"
							"Uses standard C printf syntax: %[flags][width]type\n"
							"Where 'type' is:\n"
							"    d  Signed decimal integer.\n"
							"    i  Signed decimal integer.\n"
							"    o  Unsigned octal integer.\n"
							"    u  Unsigned decimal integer.\n"
							"    x  Unsigned hexadecimal integer, using (abcdef.)\n"
							"    X  Unsigned hexadecimal integer, using (ABCDEF.)\n"
							"'flag' is:\n"
							"    -  Left align the result within the given field width.\n"
							"    +  Prefix the output value with a sign (+ or  -) if the output value is of a signed type.\n"
							"    0  If width is prefixed with 0, zeros are added until the minimum width is reached. \n"
							"    blank (' ') Prefix the output value with a blank if the output value is signed and positive.\n"
							"    #  When used with the o, x, or X format, the # flag prefixes any nonzero output value with 0, 0x, or 0X, respectively.\n"
							"'width' is an integer controlling the minimum number of characters printed.\n"
							"A %% will print a % sign\n"
							"Note: multiple % which aren't %% will crash the program.\n"
							"\nExample: %d, 0x%3x, %-05d, %#X, %02u%%...");
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK CB_FloatFormat (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	char Str[600]="", *Pos=NULL;
	
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(Pnl, PNL_FLOAT_FORMAT, Str);
			// Should also check if more than one % and the other ones are not %%
			while ((Pos=strstr(Str, "%%"))!=NULL) *Pos=*Pos++=' ';
			if ((Pos=strchr(Str, '%'))==NULL 			// No % sign
				or strchr(Pos+1, '%')!=NULL 			// More than one  
				or strpbrk (Pos+1, "eEfgG")==NULL )		// No type char following
				SetCtrlVal(Pnl, PNL_FLOAT_FORMAT, strcpy(Str, "%f"));	// Reset

			SetCtrlAttribute(Pnl, PNL_ASCII_VIEW, ATTR_VISIBLE, FALSE);
			ReadHeader();
			ReadNextRecord();
			SetCtrlAttribute(Pnl, PNL_ASCII_VIEW, ATTR_VISIBLE, TRUE);
		break;		
		case EVENT_RIGHT_CLICK:
			MessagePopup("Float output format", 
							"Format string for the real numbers output to the text file\n"
							"Uses standard C printf syntax: %[flags][width][.precision]type\n"
							"where type is:\n"
							"    e  Signed value having the form [-]d.dddde[sign]ddd where\n"
							"         d is a single decimal digit,\n"
							"         dddd is one or more decimal digits,\n"
							"         ddd is exactly three decimal digits, and sign is + or -.\n"
							"    E  Identical to the e format except that E rather than e introduces the exponent.\n"
							"    f  Signed value having the form [-]dddd.dddd, where\n"
							"         dddd is one or more decimal digits.\n"
							"         The number of digits before the decimal point depends on the magnitude of the number,\n"
							"         and the number of digits after the decimal point depends on the requested precision.\n"
							"    g  Signed value printed in f or e format, whichever is more compact for the given value and precision.\n"
							"    G  Identical to the g format, except that E, rather than e, introduces the exponent (where appropriate).\n"
							"Flag is:\n"
							"    - Left align the result within the given field width.\n"
							"    + Prefix the output value with a sign (+ or  -) if the output value is of a signed type.\n"
							"    0 If width is prefixed with 0, zeros are added until the minimum width is reached. \n"
							"    blank (' ') Prefix the output value with a blank if the output value is signed and positive.\n"
							"    # Forces a decimal point.\n"
							"Width is an integer controlling the minimum number of characters printed\n"
							"Precision specifies the number of decimal places, or the number of significant digits to print\n"
//							"For 'long double' inputs, the output format is adapted automatically.\n"
							"A %% will print a % sign\n"
							"Note: multiple % which aren't %% will crash the program.\n"
							"\nExamples: %f, %e, %-0.6#e, %5.0g, %5.2f%%...");
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK CB_StrFormat (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	char Str[600]="", *Pos=NULL;
	
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(Pnl, control, control==PNL_STR_START_FORMAT ? StrStartDelim : StrEndDelim);
			SetCtrlAttribute(Pnl, PNL_ASCII_VIEW, ATTR_VISIBLE, FALSE);
			ReadHeader();
			ReadNextRecord();
			SetCtrlAttribute(Pnl, PNL_ASCII_VIEW, ATTR_VISIBLE, TRUE);
		break;		
		case EVENT_RIGHT_CLICK:
			MessagePopup("String Delimiters", 
							"Start and end delimiters when outputing a string to the text file.\n"
							"Defaults are double quotes \". You can use more than one character.\n"
							"Example: � and �, ` and �, none...");
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
void ShowSizes(void) {
	int Nb;
	char Explain[100];
	if (SizeOfCurrentFile==0) SetCtrlVal(Pnl, PNL_SIZES, "0");
	else {
		Nb= ( FormatBufferLen==0 ? 0 : (SizeOfCurrentFile-HeaderBufferLen)/FormatBufferLen );
		sprintf(Explain, "%db = %db header + %dx%db records + %db leftover",
			SizeOfCurrentFile, HeaderBufferLen, Nb, FormatBufferLen, 
			SizeOfCurrentFile-HeaderBufferLen-Nb*FormatBufferLen);
		SetCtrlVal(Pnl, PNL_SIZES, Explain);
	}
}	

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK CB_ListType (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int Len;
	switch (event) {
		case EVENT_VAL_CHANGED:
			GetCtrlIndex(panel, control, &Index);
			SetCtrlIndex(panel, PNL_TYPE, Index);
			SetCtrlIndex(panel, PNL_HEADER, Index);
			SetCtrlIndex(panel, PNL_RECORDS, Index);
			GetLabelFromIndex(Pnl, PNL_TYPE, Index, TypeString);
			GetLabelFromIndex(Pnl, PNL_HEADER, Index, PreHeaderString);
			GetLabelFromIndex(Pnl, PNL_RECORDS, Index, PreFormatString);

			// How many char in expanded strings ?
			Len=PreExpansionCount(PreHeaderString);
			if (Len>=MaxLen) ReallocStrings(Len+1);
			Len=PreExpansionCount(PreFormatString);
			if (Len>=MaxLen) ReallocStrings(Len+1);
			Expand(HeaderString, PreHeaderString);	// remove numbers
			Expand(FormatString, PreFormatString);


			HeaderBufferLen=ExpansionCount(HeaderString);
			if (HeaderBufferLen>=MaxLen) ReallocStrings(HeaderBufferLen+1);
			SetCtrlVal(Pnl, PNL_RECORD_LENGTH, FormatBufferLen=ExpansionCount(FormatString));
			if (FormatBufferLen>=MaxLen) ReallocStrings(FormatBufferLen+1);
			
			ShowSizes();
			
			SetCtrlAttribute(Pnl, PNL_ASCII_VIEW, ATTR_VISIBLE, FALSE);
			ReadHeader();
			ReadNextRecord();
			SetCtrlAttribute(Pnl, PNL_ASCII_VIEW, ATTR_VISIBLE, TRUE);
			break;

		case EVENT_LEFT_DOUBLE_CLICK:
			GetCtrlIndex(panel, control, &Index);
			SetCtrlIndex(panel, PNL_TYPE, Index);
			SetCtrlIndex(panel, PNL_HEADER, Index);
			SetCtrlIndex(panel, PNL_RECORDS, Index);
			GetLabelFromIndex(Pnl, PNL_TYPE, Index, TypeString);
			GetLabelFromIndex(Pnl, PNL_HEADER, Index, PreHeaderString);
			GetLabelFromIndex(Pnl, PNL_RECORDS, Index, PreFormatString);

			// How many char in expanded strings ?
			Len=PreExpansionCount(PreHeaderString);
			if (Len>=MaxLen) ReallocStrings(Len+1);
			Len=PreExpansionCount(PreFormatString);
			if (Len>=MaxLen) ReallocStrings(Len+1);
			Expand(HeaderString, PreHeaderString);	// remove numbers
			Expand(FormatString, PreFormatString);

			HeaderBufferLen=ExpansionCount(HeaderString);
			if (HeaderBufferLen>=MaxLen) ReallocStrings(HeaderBufferLen+1);
			SetCtrlVal(Pnl, PNL_RECORD_LENGTH, FormatBufferLen=ExpansionCount(FormatString));
			if (FormatBufferLen>=MaxLen) ReallocStrings(FormatBufferLen+1);

			SetCtrlVal(Inp, INP_INDEX, Index);
			SetCtrlVal(Inp, INP_TYPE, TypeString);
			SetCtrlVal(Inp, INP_HEADER, PreHeaderString);
			SetCtrlVal(Inp, INP_RECORDS, PreFormatString);
			switch (control) {
				case PNL_TYPE:   SetActiveCtrl (Inp, INP_TYPE);   break;
				case PNL_HEADER: SetActiveCtrl (Inp, INP_HEADER); break;
				case PNL_RECORDS:SetActiveCtrl (Inp, INP_RECORDS);break;
			}
			InstallPopup (Inp);
			break;

		case EVENT_RIGHT_CLICK:
			MessagePopup("Format string", 
						"Format string of the binary bile\n"
						"This string specifies the kind of data present in the binary file,\n"
						"whether it is integer, float, double...\n"
						"Normally you should know the format beforehand, but with some practice,\n"
						"if you know more or less what kind of data to expect,\n"
						"you can find them by trial and error.\n\n"
						"Format of the binary file records.\n"
						"  B - one signed Byte    (INTEGER*1)\n"
						"  Y - one unsigned Byte  (INTEGER*1)\n"
						"  C - one Character      (INTEGER*1), displayed as a \"quoted string\"\n"
						"  S - one signed Short   (INTEGER*2)\n"
						"  H - one unsigned Short (INTEGER*2)\n"
						"  L - one signed Long    (INTEGER*4)\n"
						"  U - one unsigned Long  (INTEGER*4)\n"
						"  T - one signed int64   (INTEGER*8)\n"
						"  N - one unsigned int64 (INTEGER*8)\n"
						"  F - one Float          (REAL*4)\n"
						"  D - one Double float   (REAL*8)\n"
//						"  E - one long Double    (REAL*16)\n"
						"  I - Ignore one byte\n"
						"  n - repeat last letter n times\n"
						"    - spaces are ignored\n"
						"Example: \"S10 I20 F3\" to read 10 shorts, ignore 20 bytes and read 3 floats\n"
						"Note: you can specify delimiters others than double quotes in the Output Format Str boxes.");
		}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK CB_NewElement (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	#define MAX_NAME 255
	char Name[MAX_NAME+1];
	switch (event) {
		case EVENT_COMMIT:
			if (0==PromptPopup ("Name of structure to add", "", Name, MAX_NAME)) {
				InsertListItem (panel, PNL_TYPE, -1, Name, 0);
				InsertListItem (panel, PNL_HEADER, -1, "", 0);
				InsertListItem (panel, PNL_RECORDS, -1, "", 0);
				GetNumListItems (panel, PNL_TYPE, &Index);
				SetCtrlIndex(panel, PNL_TYPE, Index-=1);
				SetCtrlIndex(panel, PNL_HEADER, Index);
				SetCtrlIndex(panel, PNL_RECORDS, Index);
				HeaderString[0]='\0';
				FormatString[0]='\0';
			}
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("New format", 
						"Adds a new format string to the list\n"
						"You must give it a name, a header format string (optional)\n"
						"and a record format string.");
			break;
		}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK CB_ClearElem (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlIndex(panel, PNL_TYPE, &Index);
			if (Index!=-1) {
				DeleteListItem (panel, PNL_TYPE, Index, 1);
				DeleteListItem (panel, PNL_HEADER, Index, 1);
				DeleteListItem (panel, PNL_RECORDS, Index, 1);
			}
			break;
		}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK CB_Input (int panel, int event, void *callbackData,
		int eventData1, int eventData2) {
	int Width;
	switch (event) {
		case EVENT_PANEL_SIZE:
			GetPanelAttribute(Inp, ATTR_WIDTH, &Width);
			SetPanelAttribute(Inp, ATTR_HEIGHT, 81);
			SetCtrlAttribute(Inp, INP_HEADER, ATTR_WIDTH, Width-50);
			SetCtrlAttribute(Inp, INP_RECORDS, ATTR_WIDTH, Width-50);
			break;
	}
	return 0;
}


///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK CB_InputOK (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int Len;
	switch (event) {
		case EVENT_COMMIT:
			RemovePopup(1);
			GetCtrlAttribute (Inp, INP_TYPE, ATTR_STRING_TEXT_LENGTH, &Len);
			if (Len>=MaxLen) ReallocStrings(Len+1);
			GetCtrlAttribute (Inp, INP_HEADER, ATTR_STRING_TEXT_LENGTH, &Len);
			if (Len>=MaxLen) ReallocStrings(Len+1);
			GetCtrlAttribute (Inp, INP_RECORDS, ATTR_STRING_TEXT_LENGTH, &Len);
			if (Len>=MaxLen) ReallocStrings(Len+1);
			
			GetCtrlVal(Inp, INP_TYPE, TypeString);
			GetCtrlVal(Inp, INP_HEADER, PreHeaderString);
			GetCtrlVal(Inp, INP_RECORDS, PreFormatString);
			ReplaceListItem (Pnl, PNL_TYPE, Index, TypeString, 0);
			StringUpperCase (PreHeaderString);
			StringUpperCase (PreFormatString);
			ReplaceListItem (Pnl, PNL_HEADER, Index, PreHeaderString, 0);
			ReplaceListItem (Pnl, PNL_RECORDS, Index, PreFormatString, 0);
			
			// How many char in expanded strings ?
			Len=PreExpansionCount(PreHeaderString);
			if (Len>=MaxLen) ReallocStrings(Len+1);
			Len=PreExpansionCount(PreFormatString);
			if (Len>=MaxLen) ReallocStrings(Len+1);
			Expand(HeaderString, PreHeaderString);	// remove numbers
			Expand(FormatString, PreFormatString);
			
			HeaderBufferLen=ExpansionCount(HeaderString);
			if (HeaderBufferLen>=MaxLen) ReallocStrings(HeaderBufferLen+1);
			SetCtrlVal(Pnl, PNL_RECORD_LENGTH, FormatBufferLen=ExpansionCount(FormatString));
			if (FormatBufferLen>=MaxLen) ReallocStrings(FormatBufferLen+1);
			
			ShowSizes();
			
			SetCtrlAttribute(Pnl, PNL_ASCII_VIEW, ATTR_VISIBLE, FALSE);
			ReadHeader();
			ReadNextRecord();
			SetCtrlAttribute(Pnl, PNL_ASCII_VIEW, ATTR_VISIBLE, TRUE);
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK CB_InputCancel (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			RemovePopup(1);
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_SwapEndian (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal (Pnl, PNL_SWAP_ENDIAN, &SwapEndian);
			SetCtrlAttribute(Pnl, PNL_ASCII_VIEW, ATTR_VISIBLE, FALSE);
			ReadHeader();
			ReadNextRecord();
			SetCtrlAttribute(Pnl, PNL_ASCII_VIEW, ATTR_VISIBLE, TRUE);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Swap Endian", 
							"Use this if you want to swap the order of the bytes you read\n"
							"You should use this when reading data from a big endian machine (the PC is little endian)\n"
							"Furthermore, the element you are reading should be aligned on a boundary its own size,\n"
							"otherwise it will probably only return junk.\n"
							"\nExample: BBSFD will switch the byte order of the short, the float and the double\n"
							"BD will (probably) only return garbage in the double since it's not aligned to an 8 byte boundary.\n"
							"More explicitely, if your file specifications are really BD, it's a sign of a bad programmer\n"
							"as the result of simply putting a double after a byte are highly processor specific: there may be added bytes.\n"
							"\nA swap value of 2 will limit the swap: for instance F will swap the 1st 2 bytes and the next 2.\n"
							"\nDefault value should be 0 (for little endian) or 8 (for big endian)");
			break;
	}
	return 0;
}



///////////////////////////////////////////////////////////////////////////////
void CVICALLBACK cbm_Quit (int menuBar, int menuItem, void *callbackData, int panel) {
	QuitUserInterface(0);
}

void CVICALLBACK cbm_FileSelect (int menuBar, int menuItem, void *callbackData, int panel) {
	CB_FileSelect(Pnl, PNL_FILE_SELECT, EVENT_COMMIT, NULL, 0, 0);
}

void CVICALLBACK cbm_QuickHelp (int menuBar, int menuItem, void *callbackData, int panel) {
	MessagePopup("BinToAscii Quick Help", 
				"For detailed tutorial, go to "_WEB_
				"\nIn short proceed this way:\n"
				"- select a bunch of binary files you want to analyse,\n"
				"- create a custom format string, like LB4F4 to read a long, 4 bytes and 4 floats,\n"
				"- the prog shows the conversion of the 1st record. Examine it to see if it's right\n"
				"- adjust the output format for integer, float and string to you liking (like %5.2f),\n"
				"- run it on all files to generate a converted output.\n"
				"\nRight-clic elements of the user interface for more details.");
}

void CVICALLBACK cbm_Website (int menuBar, int menuItem, void *callbackData, int panel) {
	InetLaunchDefaultWebBrowser(_WEB_);
}

void CVICALLBACK cbm_About (int menuBar, int menuItem, void *callbackData, int panel) {
	MessagePopup("About BinToAscii", 
				"BinToAscii.exe version " _VER_ "\n"
				"Freeware "_COPY_"\n"
				"Last compiled on " __DATE__ "\n"
				"More information and tutorial at "_WEB_);
}











/*****************************************************************************/
#if 0    /* formerly excluded lines */

// This creates a test file
int main (int argc, char *argv[]) {
	int i, Sum=0, Nb=10;
	char B=10;
	short S=2000;
	long L=-3000000;
	long long N=1234567890123456789L;
	float F=44.4444F;
	double D=55.555555555555555e10;
//	long double E=66.66666666666666666666666666666666e66L;
	char Str[10][13]={"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten"};
	
	int FH = OpenFile ("Test.dat", VAL_WRITE_ONLY, VAL_TRUNCATE, VAL_BINARY);

	for (i=0; i<Nb; i++) {
		// 16 bytes
//		if (sizeof(E)!=16) Breakpoint();
//		WriteFile (FH, (char*)(void*)&E, sizeof(E)); Sum+=sizeof(E);
		
		// 8 bytes
		if (sizeof(N)!=8) Breakpoint();
		WriteFile (FH, (char*)(void*)&N, sizeof(N)); Sum+=sizeof(N);
		WriteFile (FH, (char*)(void*)&D, sizeof(D)); Sum+=sizeof(D);
		
		// 4 bytes
		WriteFile (FH, (char*)(void*)&L, sizeof(L)); Sum+=sizeof(L);
		WriteFile (FH, (char*)(void*)&F, sizeof(F)); Sum+=sizeof(F);
		
		// 2 bytes
		WriteFile (FH, (char*)(void*)&S, sizeof(S)); Sum+=sizeof(S);
		
		// 1 byte
		WriteFile (FH, (char*)&B, sizeof(B)); Sum+=sizeof(B);
		// 13 bytes to round to 16
		WriteFile (FH, Str[i], 13); Sum+=13;
		
		B++; S++; L++; N++; F++; D++; //E++;
	}
	
	printf("Written %d bytes in %d records of %d bytes\n", Sum, Nb, Sum/Nb);

	CloseFile(FH);
	return 0;
}
#endif   /* formerly excluded lines */

int CVICALLBACK cb_Sizes (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_RIGHT_CLICK:
			MessagePopup("Sizes", "Compare the size of a files to:\n"
				"- the size of a header plus\n"
				"- the size of n possible records plus\n"
				"- some possible leftover bytes (should be zero if there's no footer)");
			break;
	}
	return 0;
}

int CVICALLBACK cb_FileName(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help", "Name of the file currently being viewed.");
			break;
	}
	return 0;
}

int CVICALLBACK cb_Error(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_RIGHT_CLICK:
			MessagePopup("Error", "Lights up red if you have wrong syntax in your record definition.");
			break;
	}
	return 0;
}
