/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  INP                              1       /* callback function: CB_Input */
#define  INP_INDEX                        2       /* control type: numeric, callback function: (none) */
#define  INP_TYPE                         3       /* control type: string, callback function: (none) */
#define  INP_HEADER                       4       /* control type: string, callback function: (none) */
#define  INP_RECORDS                      5       /* control type: string, callback function: (none) */
#define  INP_OK                           6       /* control type: command, callback function: CB_InputOK */
#define  INP_CANCEL                       7       /* control type: command, callback function: CB_InputCancel */

#define  PANEL                            2
#define  PANEL_CHK_CUSTOM                 2       /* control type: radioButton, callback function: CB_Check */
#define  PANEL_HEADER_CUSTOM              3       /* control type: string, callback function: CB_Header */
#define  PANEL_FORMAT_CUSTOM              4       /* control type: string, callback function: CB_Format */
#define  PANEL_CHK_MET                    5       /* control type: radioButton, callback function: CB_Check */
#define  PANEL_HEADER_MET                 6       /* control type: string, callback function: CB_Header */
#define  PANEL_FORMAT_MET                 7       /* control type: string, callback function: CB_Format */
#define  PANEL_CHK_USA                    8       /* control type: radioButton, callback function: CB_Check */
#define  PANEL_HEADER_USA                 9       /* control type: string, callback function: CB_Header */
#define  PANEL_FORMAT_USA                 10      /* control type: string, callback function: CB_Format */

#define  PNL                              3       /* callback function: cbp_Panel */
#define  PNL_FILE_SELECT                  2       /* control type: command, callback function: CB_FileSelect */
#define  PNL_CANCEL                       3       /* control type: command, callback function: CB_Cancel */
#define  PNL_READ_ALL                     4       /* control type: command, callback function: CB_ReadAll */
#define  PNL_NEXT_REC                     5       /* control type: command, callback function: CB_NextRec */
#define  PNL_COPYRIGHT                    6       /* control type: textMsg, callback function: (none) */
#define  PNL_FILE_NAME                    7       /* control type: string, callback function: cb_FileName */
#define  PNL_SIZES                        8       /* control type: string, callback function: cb_Sizes */
#define  PNL_ERROR                        9       /* control type: LED, callback function: cb_Error */
#define  PNL_RECORD_LENGTH                10      /* control type: numeric, callback function: cb_Sizes */
#define  PNL_SWAP_ENDIAN                  11      /* control type: ring, callback function: cb_SwapEndian */
#define  PNL_TEXTMSG                      12      /* control type: textMsg, callback function: CB_ListType */
#define  PNL_QUIT                         13      /* control type: command, callback function: CB_Quit */
#define  PNL_NEW_ELEMENT                  14      /* control type: command, callback function: CB_NewElement */
#define  PNL_CLEAR_ELEM                   15      /* control type: command, callback function: CB_ClearElem */
#define  PNL_TYPE                         16      /* control type: listBox, callback function: CB_ListType */
#define  PNL_HEADER                       17      /* control type: listBox, callback function: CB_ListType */
#define  PNL_RECORDS                      18      /* control type: listBox, callback function: CB_ListType */
#define  PNL_INT_FORMAT                   19      /* control type: string, callback function: CB_IntFormat */
#define  PNL_FLOAT_FORMAT                 20      /* control type: string, callback function: CB_FloatFormat */
#define  PNL_STR_START_FORMAT             21      /* control type: string, callback function: CB_StrFormat */
#define  PNL_STR_END_FORMAT               22      /* control type: string, callback function: CB_StrFormat */
#define  PNL_SEPARATOR                    23      /* control type: binary, callback function: (none) */
#define  PNL_ASCII_VIEW                   24      /* control type: textBox, callback function: (none) */
#define  PNL_TEXTMSG_2                    25      /* control type: textMsg, callback function: (none) */


     /* Control Arrays: */

          /* (no control arrays in the resource file) */


     /* Menu Bars, Menus, and Menu Items: */

#define  MENU                             1
#define  MENU_FILE                        2
#define  MENU_FILE_SELECT                 3       /* callback function: cbm_FileSelect */
#define  MENU_FILE_QUIT                   4       /* callback function: cbm_Quit */
#define  MENU_HELP                        5
#define  MENU_HELP_QUICKHELP              6       /* callback function: cbm_QuickHelp */
#define  MENU_HELP_WEBSITE                7       /* callback function: cbm_Website */
#define  MENU_HELP_ABOUT                  8       /* callback function: cbm_About */


     /* Callback Prototypes: */

int  CVICALLBACK CB_Cancel(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_Check(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_ClearElem(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Error(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_FileName(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_FileSelect(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_FloatFormat(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_Format(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_Header(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_Input(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_InputCancel(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_InputOK(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_IntFormat(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_ListType(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_NewElement(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_NextRec(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_Quit(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_ReadAll(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Sizes(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_StrFormat(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_SwapEndian(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
void CVICALLBACK cbm_About(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_FileSelect(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_QuickHelp(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_Quit(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_Website(int menubar, int menuItem, void *callbackData, int panel);
int  CVICALLBACK cbp_Panel(int panel, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif