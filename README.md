PROGRAM:    BinToAscii.exe                                                        

AUTHOR:     Guillaume Dargaud                                 

PURPOSE:    Convert a binary file containing integers/floats with a classic sequential structure such as header/Record1/Record2... to an ascii file.

INSTALL:    run SETUP.   
            You NEED to install the LabWindows/CVI runtime engine on your PC, it is available from National Instruments

HELP:       available by right-clicking an item on the user interface.

TUTORIAL:   http://www.gdargaud.net/Hack/BinToAscii.html